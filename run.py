import config
from app.server import server
from app.app import app


if __name__ == '__main__':
    server.run(port=8050, debug=True)
