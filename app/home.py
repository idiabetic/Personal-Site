import dash_core_components as dcc
import dash_html_components as html


HomeLayout = (

    html.Div([
        html.Div([
            html.H3("Hi, I'm",
                    style={'color': '#d9b310',
                           'font-weight': 'bold',
                           'font-size': '60px',
                           'margin-bottom': '5px',
                           'margin-top': '40px',
                           'margin-left': '20px',
                           'font-style': 'italic',
                           'text-align': 'center'}),
            html.H1('Patrick Shobe',
                    style={
                           'margin-left': '20px',
                           'font-size': '100px',
                           'font-weight': '400',
                           'color': '#d9b310',
                           'text-align': 'center'}),
            html.H4('Chronic Learner',
                    style={
                        'border-left': '4px solid #D9B310',
                        'padding-left': '10px',
                        'font-size': '40px',
                        'font-weight': '200px',
                        'color': '#2c3e50',
                        'margin-left': '210px',
                        'margin-bottom': '5px',

                    }),
            html.H4('Problem Solver',
                    style={
                        'font-size': '40px',
                        'font-weight': '200px',
                        'color': '#2c3e50',
                        'margin-left': '210px',
                        'margin-top': '5px',
                        'margin-bottom': '5px',
                        'border-left': '4px solid #D9B310',
                        'padding-left': '10px', }),
            html.H4('Data Evangelist',
                    style={
                        'font-size': '40px',
                        'font-weight': '200px',
                        'color': '#2c3e50',
                        'margin-left': '210px',
                        'margin-top': '5px',
                        'border-left': '4px solid #D9B310',
                        'padding-left': '10px', }),
        ], id='Hello-Div',
            style={
            'justify-self': 'center',
            'align-self': 'center',


            }),

    ], id='Top-Div',
        style={
                'display': 'grid',

                'height': '1000px',
                'margin-left': '-8px',
                'background-image':
                'url(https://image.ibb.co/c70Onn/Plot_1_1_1.jpg)',
                # 'url(https://image.ibb.co/dSoCf7/pexels_photo_276517.jpg)',
                'background-repeat': 'no-repeat',
                'background-attachment': 'scroll',
                'background-position': 'center',
                'background-size': '100% 100%',
                'width': '100vw',
                'margin-top': '0px',

        }),

    html.Div([
        html.H2("- What I've Done So Far -",
                style={
                    'text-align': 'center',
                    'vertical-align': 'center',
                    'font-size': '50px'
                })
    ], style={
        'display': 'grid',
        'height': '150px',
        'width': '100vw',
        'padding-top': '50px',

    }),

    html.Div([
        html.Div([
            html.Img(src='https://static1.squarespace.com/static/5936a5346b8' +
                         'f5bd45d128f12/t/597261f1f14aa1811d00bd38/15006684' +
                         '04424/?format=750w')
        ], style={
            'padding-top': '25px',
            'padding-bottom': '25px',
            'height': '300px',
            'padding-left': '20px',
            'border-radius': '15px',
        }),

        html.Div([
            html.H2('TRELORA Real Estate',
                    style={
                        'color': '#D9b310',
                        'font-size': '35px',
                        'margin-bottom': '0px'
                    }),
            html.H3('Data Analyst, Since April 2015',
                    style={
                        'font-size': '25px',
                        'margin-top': '5px',
                        'margin-bottom': '5px'
                    }),
            html.H4('Denver, CO',
                    style={
                        'font-size': '22px',
                        'color': '#333c45',
                        'margin-top': '5px'
                    }),
            html.P('TRELORA is a residential real estate startup utilizing ' +
                   'a flat fee commission model to revolutionize the ' +
                   'industry. I created and developed the data analyst role ' +
                   'within TRLEORA and helped steer the company to a truly ' +
                   'data driven organization',
                   style={
                    'font-size': '20px',
                    'margin-top': '5px'
                   })
        ], style={
            'padding-bottom': '25px',
        }),

        html.Div([
            html.Img(src='https://image.ibb.co/cy783S/oil_pump_jack_sunset_' +
                         'clouds_silhouette_162568.jpg',
                         style={
                            'width': '400px',
                            'border-radius': '20px', })
        ], style={
            'padding-top': '25px',
            'padding-bottom': '25px',
            'height': '300px'}),

        html.Div([
            html.H2('Black Sand Resources',
                    style={
                        'color': '#D9b310',
                        'font-size': '35px',
                        'margin-bottom': '0px'
                        }),
            html.H3('Petroleum Geo Tech, January 2012 - March 2015',
                    style={
                        'font-size': '25px',
                        'margin-top': '5px',
                        'margin-bottom': '5px'
                    }),
            html.H4('Denver, CO',
                    style={
                        'font-size': '22px',
                        'color': '#333c45',
                        'margin-top': '5px'
                    }),
            html.P('Black Sand Resources LLC is a small independent oil and ' +
                   'gas consultancy specializing in prospect analysis and ' +
                   'creation in Kansas, Oklahoma, and the Illinois Basin. I ' +
                   'interpreted geologic and seismic data in order to ' +
                   'successfully prospect for liquid and gas hydrocarbons, ' +
                   'performed due diligence to determine the likelihood of ' +
                   'success of outside projects, conveyed prospects and ' +
                   'scientific data to prospective investors. ',
                   style={
                    'font-size': '20px',
                    'margin-top': '5px'
                   })
        ], style={
            'padding-bottom': '25px',
        }),

        html.Div([
            html.Img(src='https://image.ibb.co/fiF0Hn/pexels_photo_69004.jpg',
                     style={
                            'width': '400px',
                            'border-radius': '20px',
                        })
        ], style={
            'padding-top': '25px',
            'padding-bottom': '25px',
            'height': '300px'}),

        html.Div([
            html.H2('Hammerhead Resources',
                    style={
                        'color': '#D9b310',
                        'font-size': '35px',
                        'margin-bottom': '0px'
                    }),
            html.H3('Petroleum Landman, May 2010 - January 2012',
                    style={
                        'font-size': '25px',
                        'margin-top': '5px',
                        'margin-bottom': '5px'
                    }),
            html.H4('Wichita, KS',
                    style={
                        'font-size': '22px',
                        'color': '#333c45',
                        'margin-top': '5px'
                    }),
            html.P('Hammerhead Resources is a small petroleum leasing ' +
                   'agency specializing in northern Oklahoma and Kansas. I ' +
                   'conducted subsurface mineral title ownership research, ' +
                   'drafted contracts associated with oil and gas leasing, ' +
                   'managed projects ranging in size from 1 to 30 square ' +
                   'miles and managed multiple projects concurrently.',
                   style={
                    'font-size': '20px',
                    'margin-top': '5px'
                   })
        ], style={
            'padding-bottom': '25px',
        }),
            ], style={
                'display': 'grid',
                'grid-template-columns': '30% 70%',
                'margin-left': '150px',
                'margin-right': '150px'
    }),

    html.Div([
        html.Div([
            html.H2("- Technical Skills -",
                    style={
                        'text-align': 'center',
                        'vertical-align': 'center',
                        'font-size': '50px',
                        'color': '#F7F7F7',
                        })
                        ], style={
                        'display': 'grid',
                        'height': '150px',
                        'width': '100vw',
                        'padding-top': '40px',
                        'margin-bottom': '-50px',

                        }),

        html.Div([
            html.Div([
                html.H2('Python',
                        style={
                            'color': '#D9B310',
                            'border-bottom': '4px solid #328cc1',
                            'margin-right': '40px',
                            'margin-left': '40px',
                            'padding-bottom': '15px'
                        }),
                html.P('Python is the most widely used language for data ' +
                       'science and analysis. Packages like Pandas, Numpy, '
                       'and SQLalchemy allow for fast pased data ' +
                       'manipulation and analysis',
                       style={
                        'font-size': '25px',
                        'margin-left': '15px',
                        'margin-right': '15px',
                        'margin-top': '15px',
                       }),
            ],
                style={
                    'width': '70%',
                    'height': '80%',
                    'margin-left': '150px',
                    'background-color': '#FFFFFF',
                    'box-shadow': '0 1px 3px rgba(0,0,0,0.12),' +
                                  '0 1px 2px rgba(0,0,0,0.24)',
                    'text-align': 'center',
                    'border-radius': '10px'
                }),
            html.Div([
                html.H2('Dash',
                        style={
                            'color': '#D9B310',
                            'border-bottom': '4px solid #328cc1',
                            'margin-right': '40px',
                            'margin-left': '40px',
                            'padding-bottom': '15px'
                        }),
                html.P('Dash is a Python framework for building analytical ' +
                       'web applications based on the Flask framework. This ' +
                       'allows complex analysis and ideas to be conveyed in ' +
                       'a beautiful and easily digestable format',
                       style={
                        'font-size': '25px',
                        'margin-left': '15px',
                        'margin-right': '15px',
                        'margin-top': '15px'
                       })
            ],
                style={
                    'width': '70%',
                    'height': '80%',
                    'background-color': '#FFFFFF',
                    'box-shadow': '0 1px 3px rgba(0,0,0,0.12),' +
                                  '0 1px 2px rgba(0,0,0,0.24)',
                    'text-align': 'center',
                    'border-radius': '10px'
                }),
            html.Div([
                html.H2('Tableau',
                        style={
                            'color': '#D9B310',
                            'border-bottom': '4px solid #328cc1',
                            'margin-right': '40px',
                            'margin-left': '40px',
                            'padding-bottom': '15px'
                        }),
                html.P('Tableau is a software product that allows for fast ' +
                       'pased data analytics and analysis without needing ' +
                       'to know any programming languages',
                       style={
                        'font-size': '25px',
                        'margin-left': '15px',
                        'margin-right': '15px',
                        'margin-top': '15px'
                       })
            ],
                style={
                    'width': '70%',
                    'height': '80%',
                    'margin-right': '150px',
                    'background-color': '#FFFFFF',
                    'box-shadow': '0 1px 3px rgba(0,0,0,0.12),' +
                                  '0 1px 2px rgba(0,0,0,0.24)',
                    'text-align': 'center',
                    'border-radius': '10px'
                }),

            html.Div([
                html.H2('SQL',
                        style={
                            'color': '#D9B310',
                            'border-bottom': '4px solid #328cc1',
                            'margin-right': '40px',
                            'margin-left': '40px',
                            'padding-bottom': '15px'
                        }),
                html.P('SQL is a standard language for storing, ' +
                       'manipulating and retrieving data in databases. I ' +
                       'am proficient in MySQL and Postgres',
                       style={
                        'font-size': '25px',
                        'margin-left': '15px',
                        'margin-right': '15px',
                        'margin-top': '15px'
                       })
            ],
                style={
                    'width': '70%',
                    'height': '80%',
                    'margin-left': '150px',
                    'background-color': '#FFFFFF',
                    'box-shadow': '0 1px 3px rgba(0,0,0,0.12),' +
                                  '0 1px 2px rgba(0,0,0,0.24)',
                    'text-align': 'center',
                    'border-radius': '10px'
                }),
            html.Div([
                html.H2('Flask',
                        style={
                            'color': '#D9B310',
                            'border-bottom': '4px solid #328cc1',
                            'margin-right': '40px',
                            'margin-left': '40px',
                            'padding-bottom': '15px'
                            }),
                html.P('Flask is a micro web framework written in python ' +
                       'designed to build fully functional web apps',
                       style={
                        'font-size': '25px',
                        'margin-left': '15px',
                        'margin-right': '15px',
                        'margin-top': '15px'
                       })
            ],
                style={
                    'width': '70%',
                    'height': '80%',
                    'background-color': '#FFFFFF',
                    'box-shadow': '0 1px 3px rgba(0,0,0,0.12),' +
                                  '0 1px 2px rgba(0,0,0,0.24)',
                    'text-align': 'center',
                    'border-radius': '10px'
                        }),
            html.Div([
                    html.H2('HTML/CSS',
                            style={
                                'color': '#D9B310',
                                'border-bottom': '4px solid #328cc1',
                                'margin-right': '40px',
                                'margin-left': '40px',
                                'padding-bottom': '15px'
                            }),
                    html.P('HTML (the Hypertext Markup Language) and CSS ' +
                           '(Cascading Style Sheets) are two of the core ' +
                           'technologies for building Web pages.',
                           style={
                            'font-size': '25px',
                            'margin-left': '15px',
                            'margin-right': '15px',
                            'margin-top': '15px'
                           })
            ],
                style={
                    'width': '70%',
                    'height': '80%',
                    'margin-right': '150px',
                    'background-color': '#FFFFFF',
                    'box-shadow': '0 1px 3px rgba(0,0,0,0.12),' +
                                  '0 1px 2px rgba(0,0,0,0.24)',
                    'text-align': 'center',
                    'border-radius': '10px'
                }),

        ], style={
            'display': 'grid',
            'grid-template-columns': '33% 33% 33% ',
            'height': '800px',
            'width': '100vw',
            'justify-items': 'center',
            'align-items': 'center'
            }),
    ], style={
        'background-color': '#0b3c5d'
    }),

    html.Div([
        html.Div([
            html.Div([
                html.H2('- Projects -',
                        style={
                            'text-align': 'center',
                            'vertical-align': 'center',
                            'font-size': '50px', })
                            ], style={
                            'display': 'grid',
                            'height': '150px',
                            'width': '100vw',
                            'padding-top': '40px',
                            'margin-bottom': '-50px', })
            ]),

        html.Div([
            html.H3('Under Construction',
                    style={
                        'text-align': 'center',
                        'vertical-align': 'center',
                        'font-size': '60px',
                        'padding-top': '20%',
                        'color': '#d9b310',
                        'font-weight': '700'
                        })
            ], style={
                'width': '70%',
                'height': '80%',


                'box-shadow': '0 1px 3px rgba(0,0,0,0.24),' +
                              '0 1px 2px rgba(0,0,0,0.36)',
                'text-align': 'center',
                'border-radius': '10px',
                'justify-items': 'center',
                'align-items': 'center',
                'margin-left': '15%',
                'background-image':
                'url(https://image.ibb.co/dZ10Hn/mmm.jpg)',

                'background-repeat': 'no-repeat',
                'background-attachment': 'scroll',
                'background-position': 'cover',
                'background-size': '100% 100%', }),
            ],
                style={
                'height': '800px'
                }),

    html.Div([
        html.Div([
            html.H2('- Contact -',
                    style={
                        'text-align': 'center',
                        'vertical-align': 'center',
                        'font-size': '50px', })
        ], style={
                'display': 'grid',
                'height': '150px',
                'width': '100vw',
                'padding-top': '40px',
                'margin-bottom': '0px',
        }),

        html.Div([
            html.Div([
                html.Img(src='https://image.ibb.co/nx74A7/iconmonstr_phone' +
                             '_1_240.png'),
            ]),

            html.Div([
                html.Img(src='https://image.ibb.co/ktOVHn/iconmonstr_email_' +
                     '11_240.png'),

            ]),

            html.Div([
                html.Img(src='https://image.ibb.co/ceyrOS/git2.png'),

            ]),

            html.Div([
                dcc.Link('303.304.9045',
                     href='tel:+13033049045',
                     style={
                        'font-size': '22px',
                     }),
            ]),

            html.Div([
                dcc.Link('PoShobe@gmail.com',
                         href='mailto:poshobe@gmail.com',
                         style={
                            'font-size': '22px',
                         }),
            ]),

            html.Div([
                dcc.Link('Github',
                     href='https://github.com/patrickshobe',
                     style={
                        'font-size': '22px',
                     }),
            ])
        ], style={
                'display': 'grid',
                'grid-template-columns': '33% 33% 33% ',
                'height': '400px',
                'width': '50%',
                'justify-items': 'center',
                'align-items': 'center',
                'margin-left': '25%'


        })
    ]),


        )
