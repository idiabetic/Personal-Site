import dash_html_components as html


MissingLayout = (
        html.Div([
            html.H1('Oops, Looks like you got lost!'),
            html.A('Take me Home!',
                   href='/')
        ]))
