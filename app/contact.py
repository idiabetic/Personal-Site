import dash_core_components as dcc
import dash_html_components as html


ContactLayout = (
        html.Div([

            html.Div([
                html.Div([
                    dcc.Link('Resume', href='/resume')
                ], id='Resume-Link-Div'),
                html.Div([
                    dcc.Link('Skills', href='/skills')
                ], id='Skills-Link-Div'),
                html.Div([
                    dcc.Link('Projects', href='/projects')
                ], id='Projects-Link-Div'),
            ], id='Left-Links-Div'),

            html.Div([
                    dcc.Link('Patrick Shobe', href='/')
            ], id='Name-Div'),

            html.Div([
                dcc.Link('Contact', href='/contact')
            ], id='Contact-Div')
        ], id='Top-Bar-Div'),

        html.Div([
            html.Div([
                    html.A(
                        'Github',
                        href='https://github.com/patrickshobe')
            ], id='Github-Div'),

            html.Div([
                html.A(
                    'Email',
                    href='mailto:poshobe@gmail.com')
            ], id='Email-Div'),

            html.Div([
                html.A(
                    'Phone',
                    href='tel:+13033049045">303-304-9045'
                )
            ], id='Phone-Div')
        ], id='Contact-Div')
        )
