# -*- coding: utf-8 -*-
import os
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output


from .server import server
from .home import HomeLayout
from .projects import ProjectsLayout
from .missing import MissingLayout


app = dash.Dash(server=server)
app.config['suppress_callback_exceptions'] = True


@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/':
        return HomeLayout
    elif pathname == '/projects':
        return ProjectsLayout
    else:
        return MissingLayout


app.layout = (html.Div(children=[

    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content'),
    ]))

app.css.append_css({
    'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css',
})
