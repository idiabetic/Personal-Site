import os


PROJECT_ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
PROJECT_NAME = os.path.basename(PROJECT_ROOT_DIR)

os.environ['%s_PROJECT_ROOT_DIR' % PROJECT_NAME] = PROJECT_ROOT_DIR
os.environ['DASH_PROJECT_NAME'] = PROJECT_NAME
